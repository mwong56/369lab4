import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.QueryBuilder;
import com.mongodb.client.*;

import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import javax.print.Doc;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientSpoof {
  private static JSONObject config;

  static String server;
  static Integer port;

  static String dbName;
  static String collectionName;
  static String monitorCollName;
  static Integer delayAmount;

  static File wordFile;
  static File clientLogFile;
  static File serverLogFile;
  static File queryWordFile;

  static long lastCheckpoint = 0;

  public static void main(String[] args) throws JSONException, FileNotFoundException {
    getConfigFile(args);
    parseConfig();
    startClient();
  }

  public static void startClient() {
    new Thread(new Runnable() {
      @Override
      public void run() {
        PrintWriter writer = null;
        try {
          writer = new PrintWriter(clientLogFile);
        } catch (Exception e) {
          throwError("File not found");
        }

        Date date = new Date();
        printLog(writer, "Current timestamp: " + new Timestamp(date.getTime()));

        MongoDatabase db = null;
        try {
          Logger logger = Logger.getLogger("org.mongodb.driver");  // turn off logging
          logger.setLevel(Level.OFF);
          MongoClient c = new MongoClient(server, port);
          db = c.getDatabase(dbName);
          printLog(writer, "Mongo server: " + server + ":" + port);
          printLog(writer, "Database name: " + dbName);
          printLog(writer, "Collection name: " + collectionName);
        } catch (Exception e) {
          throwError("Error connecting to mongo");
        }

        MongoCollection<Document> collection = db.getCollection(collectionName);
        long count = collection.count();
        MessageLog.messageCount = (int) count;
        printLog(writer, "Total items in collection: " + count);

        ArrayList<String> dictionary = createDictionary(wordFile);
        int cycles = 0;
        String lastUser = "";


        while (true) {
          try {
            Thread.sleep(delayAmount * 1000);
          } catch (Exception e) {
            throwError("Error at sleeping thread");
          }

          if (cycles == 40) {
            printLog(writer, "Total number of messages in collection: " + collection.count());
            Document query = new Document();
            query.append("user", lastUser);
            printLog(writer, "Total number of messages written by " + lastUser + ": " + collection.count(query));
            cycles = 0;
          }
          MessageLog log = new MessageLog(dictionary);
          Document document = Document.parse(log.toJSONObject().toString());
          collection.insertOne(document);
          lastUser = log.getUserID();
          date = new Date();
          printLog(writer, new Timestamp(date.getTime()) + ": " + log.toString());
          cycles++;
        }
      }
    }).start();
  }

  private static void printLog(PrintWriter writer, String log) {
    writer.println(log);
    writer.flush();
    System.out.println(log);
  }

  public static void getConfigFile(String[] args) {
    if (args.length < 1) {
      throwError("Args 1 needs to be config file.");
    }
    File configFile = new File(args[0]);
    try {
      Scanner scanner = new Scanner(configFile);
      String jsonString = "";
      while (scanner.hasNext()) {
        jsonString += scanner.next();
      }
      config = new JSONObject(jsonString);
    } catch (Exception e) {
      throwError("Can't find config file.");
    }
  }

  static void parseConfig() {
    server = config.getString("mongo");
    if (server == null && server.length() > 0) {
      server = "localhost";
    }

    port = config.getInt("port");
    if (port == null) {
      port = 27017;
    }

    dbName = config.getString("database");
    collectionName = config.getString("collection");
    monitorCollName = config.getString("monitor");
    delayAmount = config.getInt("delay");
    if (delayAmount == null || delayAmount == 0) {
      delayAmount = 10;
    }

    wordFile = new File(config.getString("words"));
    clientLogFile = new File(config.getString("clientLog"));
    serverLogFile = new File(config.getString("serverLog"));
    queryWordFile = new File(config.getString("wordFilter"));
  }


  public static ArrayList<String> createDictionary(File file) {
    ArrayList<String> toReturn = new ArrayList<>();
    Scanner scan = null;
    try {
      scan = new Scanner(file);
    } catch (Exception e) {
      System.err.println("Invalid dictionary file.");
      System.exit(0);
    }

    while (scan.hasNext()) {
      toReturn.add(scan.next());
    }

    return toReturn;
  }

  private static void throwError(String error) {
    System.out.println(error);
    System.exit(0);
  }
}
