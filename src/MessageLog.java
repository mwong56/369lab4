import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

public class MessageLog {
  private static final String stat1 = "public";
  private static final String stat2 = "protected";
  private static final String stat3 = "private";
  private static final String recep1 = "all";
  private static final String recep2 = "self";
  private static final String recep3 = "subscribers";
  public static int messageCount;
  private static Random rand = new Random();

  private int messageID;
  private String userID;
  private String messageStatus;
  private String recepient;
  private String messageText = "";
  private int originalMessageID = -1;

  public MessageLog(ArrayList<String> dictionary) {
    messageID = messageCount++;
    userID = generateUserID();
    messageStatus = generateMessageStatus();
    recepient = generateRecepient();
    messageText = generateText(dictionary);
    if (rand.nextInt(2) == 1) {
      originalMessageID = rand.nextInt(messageID);
    }
  }

  public String getUserID() {
    return userID;
  }

  private String generateUserID() {
    return "u" + rand.nextInt(10000) + 1;
  }

  private String generateMessageStatus() {
    int x = rand.nextInt(101);
    if (x <= 75) {
      return stat1; //public
    } else if (x > 75 && x < 88) {
      return stat2; //protected
    } else {
      return stat3; //private
    }
  }

  private String generateRecepient() {
    String toReturn = "";
    if (messageStatus.equals("private")) {
      if (rand.nextInt(2) == 0) {
        toReturn = recep2; //self
      } else {
        toReturn = generateUserID();
        while (toReturn.equals(userID)) {
          toReturn = generateUserID();
        }
      }
    } else if (messageStatus.equals("protected")) {
      int x = rand.nextInt(101);
      if (x == 75) {
        toReturn = recep3; //subscribers
      } else if (x > 76 && x < 88) {
        toReturn = recep2; //self
      } else {
        toReturn = generateUserID();
        while (toReturn.equals(userID)) {
          toReturn = generateUserID();
        }
      }
    } else if (messageStatus.equals("public")) {
      int x = rand.nextInt(101);
      if (x <= 40) {
        toReturn = recep1; //all
      } else if (x > 40 && x <= 80) {
        toReturn = recep3; //subscribers
      } else if (x > 80 && x <= 90) {
        toReturn = recep2; //self
      } else {
        toReturn = generateUserID();
        while (toReturn.equals(userID)) {
          toReturn = generateUserID();
        }
      }
    }
    return toReturn;
  }

  private String generateText(ArrayList<String> dictionary) {
    int numWords = rand.nextInt(19) + 2;
    String toReturn = "";
    for (int i = 0; i < numWords; i++) {
      int whichWord = rand.nextInt(dictionary.size() - 1);
      toReturn += dictionary.get(whichWord) + " ";
    }
    return toReturn;
  }

  public JSONObject toJSONObject() throws JSONException {
    JSONObject toReturn = new JSONObject();
    toReturn.put("messageId", messageID);
    toReturn.put("user", userID);
    toReturn.put("status", messageStatus);
    toReturn.put("recepient", recepient);
    toReturn.put("text", messageText);
    if (originalMessageID > 0) {
      toReturn.put("in-response", originalMessageID);
    }
    return toReturn;
  }

  @Override
  public String toString() {
    return "\n     messageID=" + messageID +
        "\n     userID='" + userID + '\'' +
        "\n     messageStatus='" + messageStatus + '\'' +
        "\n     recepient='" + recepient + '\'' +
        "\n     messageText='" + messageText + '\'' +
        "\n     in-response=" + originalMessageID +
        "\n";
  }
}

