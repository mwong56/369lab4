import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.QueryBuilder;
import com.mongodb.client.*;

import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import javax.print.Doc;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerSpoof {
    private static JSONObject config;

    static String server;
    static Integer port;

    static String dbName;
    static String collectionName;
    static String monitorCollName;
    static Integer delayAmount;

    static File wordFile;
    static File clientLogFile;
    static File serverLogFile;
    static File queryWordFile;

    static long lastCheckpoint = 0;

    public static void main(String[] args) throws JSONException, FileNotFoundException {
        getConfigFile(args);
        parseConfig();
        startServer();
    }

    public static void startServer() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                PrintWriter writer = null;
                try {
                    writer = new PrintWriter(serverLogFile);
                } catch (Exception e) {
                    throwError("File not found");
                }

                Date date = new Date();
                System.out.println("Current timestamp: " + new Timestamp(date.getTime()));

                MongoDatabase db = null;
                try {
                    Logger logger = Logger.getLogger("org.mongodb.driver");  // turn off logging
                    logger.setLevel(Level.OFF);
                    MongoClient c = new MongoClient(server, port);
                    db = c.getDatabase(dbName);
                    System.out.println("Mongo server: " + server + ":" + port);
                    System.out.println("Database name: " + dbName);
                    System.out.println("Collection name: " + collectionName);
                } catch (Exception e) {
                    throwError("Error connecting to mongo");
                }

                final MongoCollection<Document> collection = db.getCollection(collectionName);
                System.out.println("Total items in collection: " + collection.count() + "\n");

                BasicDBObject basicDBObject = new BasicDBObject();
                basicDBObject.append("text", "text");
                collection.createIndex(basicDBObject);

                db.getCollection(monitorCollName).drop();
                MongoCollection<Document> monitorCollection = db.getCollection(monitorCollName);

                ArrayList<String> queryWords = createDictionary(queryWordFile);

                while (true) {
                    try {
                        Thread.sleep(3 * delayAmount * 1000);
                    } catch (Exception e) {
                        throwError("Error at sleeping thread");
                    }

                    long numUniqueMessages = 0;
                    MongoCursor crsr = collection.distinct("text", String.class).iterator();
                    while(crsr.hasNext()) {
                        crsr.next();
                        numUniqueMessages++;
                    }

                    System.out.println("Number of unique messages: " + numUniqueMessages);

                    final Set<String> uniqueUsers = new HashSet<String>();
                    FindIterable<Document> iter = collection.find(new Document("user", new Document("$exists", true)));
                    iter.forEach(new Block<Document>() {
                        @Override
                        public void apply(final Document document) {
                            uniqueUsers.add(document.getString("user"));
                        }
                    });
                    System.out.println("Number of unique users: " + uniqueUsers.size());

                    final long numMessageSinceCheckPoint = collection.count() - lastCheckpoint;
                    System.out.println("Number of messages inserted since last checkpoint: " + numMessageSinceCheckPoint);

                    final long[] privateCtr = {0};
                    final long[] protectedCtr = {0};
                    final long[] publicCtr = {0};
                    iter = collection.find(new Document("status", new Document("$exists", true)));
                    iter.forEach(new Block<Document>() {
                        @Override
                        public void apply(final Document document) {
                            if (document.getString("status").equals("public")) {
                                publicCtr[0]++;
                            } else if (document.getString("status").equals("protected")) {
                                protectedCtr[0]++;
                            } else {
                                privateCtr[0]++;
                            }
                        }
                    });
                    System.out.println("Number of public messages: " + publicCtr[0]);
                    System.out.println("Number of protected messages: " + protectedCtr[0]);
                    System.out.println("Number of private messages: " + privateCtr[0]);

                    final long[] selfCtr = {0};
                    final long[] allCtr = {0};
                    final long[] subscribersCtr = {0};
                    final long[] usersCtr = {0};
                    iter = collection.find(new Document("recepient", new Document("$exists", true)));
                    iter.forEach(new Block<Document>() {
                        @Override
                        public void apply(final Document document) {
                            if (document.getString("recepient").equals("self")) {
                                selfCtr[0]++;
                            } else if (document.getString("recepient").equals("all")) {
                                allCtr[0]++;
                            } else if (document.getString("recepient").equals("subscribers")) {
                                subscribersCtr[0]++;
                            } else {
                                usersCtr[0]++;
                            }
                        }
                    });
                    System.out.println("Number of messages with recepient of all: " + allCtr[0]);
                    System.out.println("Number of messages with recepient of self: " + selfCtr[0]);
                    System.out.println("Number of messages with recepient of subscribers: " + subscribersCtr[0]);
                    System.out.println("Number of messages with recepient of specific user: " + usersCtr[0]);

                    System.out.println();

                    if (lastCheckpoint > 0) {
                        for (final String word : queryWords) {
                            DBObject result = QueryBuilder.start().text(word).put("messageId").greaterThan(lastCheckpoint).get();
                            final Document doc = Document.parse(result.toString());
                            if (collection.count(doc) > 0) {
                                System.out.println("2");
                                System.out.println("Word search, keyword: " + word);
                                iter = collection.find(doc);
                                iter.forEach(new Block<Document>() {
                                    @Override
                                    public void apply(final Document document) {
                                        System.out.println("  text=" + document.get("text") + ", recepient=" + document.get("recepient"));
                                        System.out.println("    status=" + document.get("status"));
                                        System.out.println("    messageId=" + document.get("messageId"));
                                        System.out.println("    user=" + document.get("user"));
                                    }
                                });
                                System.out.println();
                            }
                        }
                    }

                    JSONObject monitorDoc = new JSONObject();
                    date = new Date();
                    monitorDoc.put("time", new Timestamp(date.getTime()));
                    monitorDoc.put("messages", numUniqueMessages);
                    monitorDoc.put("users", uniqueUsers.size());
                    monitorDoc.put("new", numMessageSinceCheckPoint);

                    JSONArray statusStats = new JSONArray();
                    JSONObject publicStatus = new JSONObject();
                    publicStatus.put("public", publicCtr[0]);
                    JSONObject protectedStatus = new JSONObject();
                    protectedStatus.put("protected", protectedCtr[0]);
                    JSONObject privateStatus = new JSONObject();
                    privateStatus.put("private", privateCtr[0]);
                    statusStats.put(publicStatus);
                    statusStats.put(protectedStatus);
                    statusStats.put(privateStatus);
                    monitorDoc.put("statusStats", statusStats);

                    JSONArray recepientStats = new JSONArray();
                    JSONObject selfStatus = new JSONObject();
                    selfStatus.put("self", selfCtr[0]);
                    JSONObject allStatus = new JSONObject();
                    allStatus.put("all", allCtr[0]);
                    JSONObject subscribersStatus = new JSONObject();
                    subscribersStatus.put("subscribers", subscribersCtr[0]);
                    JSONObject usersStatus = new JSONObject();
                    usersStatus.put("users", usersCtr[0]);
                    recepientStats.put(selfStatus);
                    recepientStats.put(allStatus);
                    recepientStats.put(subscribersStatus);
                    recepientStats.put(usersStatus);
                    monitorDoc.put("recepientStats", recepientStats);

                    Document document = Document.parse(monitorDoc.toString());
                    monitorCollection.insertOne(document);

                    writer.println(document.toJson());
                    writer.flush();

                    //SR8
                    if(lastCheckpoint == 0) {
                        Document monitorTotals = new Document();
                        monitorTotals.put("recordType", "monitor totals");
                        List<Long> count = new ArrayList<>();
                        count.add(collection.count());
                        monitorTotals.put("msgTotals", count);
                        List<Integer> numUsers = new ArrayList<>();
                        numUsers.add(uniqueUsers.size());
                        monitorTotals.put("userTotals", numUsers);
                        List<Long> messages = new ArrayList<>();
                        messages.add(numMessageSinceCheckPoint);
                        monitorTotals.put("newMsgTotals", messages);
                        monitorCollection.insertOne(monitorTotals);
                    } else {
                        Document query = new Document();
                        query.put("recordType", "monitor totals");
                        iter = monitorCollection.find(query);

                        final Document newMonitorTotals = new Document();
                        newMonitorTotals.put("recordType", "monitor totals");

                        iter.forEach(new Block<Document>() {
                            @Override
                            public void apply(final Document document) {
                                List<Long> newCount = new ArrayList<Long>();
                                if (document.get("msgTotals") instanceof List) {
                                    newCount = (List<Long>) document.get("msgTotals");
                                    newCount.add(collection.count());
                                    newMonitorTotals.put("msgTotals", newCount);
                                }

                                List<Integer> newUsers = new ArrayList<Integer>();
                                if (document.get("userTotals") instanceof List) {
                                    newUsers = (List<Integer>) document.get("userTotals");
                                    newUsers.add(uniqueUsers.size());
                                    newMonitorTotals.put("userTotals", newUsers);
                                }

                                List<Long> newMsgTotals = new ArrayList<Long>();
                                if(document.get("newMsgTotals") instanceof List) {
                                    newMsgTotals = (List<Long>) document.get("newMsgTotals");
                                    newMsgTotals.add(numMessageSinceCheckPoint);
                                    newMonitorTotals.put("newMsgTotals", newMsgTotals);
                                }
                            }
                        });
                        monitorCollection.findOneAndDelete(query);
                        monitorCollection.insertOne(newMonitorTotals);
                    }
                    lastCheckpoint = collection.count();
                }
            }
        }).start();
    }

    private static void printLog(PrintWriter writer, String log) {
        writer.println(log);
        writer.flush();
        System.out.println(log);
    }

    public static void getConfigFile(String[] args) {
        if (args.length < 1) {
            throwError("Args 1 needs to be config file.");
        }
        File configFile = new File(args[0]);
        try {
            Scanner scanner = new Scanner(configFile);
            String jsonString = "";
            while (scanner.hasNext()) {
                jsonString += scanner.next();
            }
            config = new JSONObject(jsonString);
        } catch (Exception e) {
            throwError("Can't find config file.");
        }
    }

    static void parseConfig() {
        server = config.getString("mongo");
        if (server == null && server.length() > 0) {
            server = "localhost";
        }

        port = config.getInt("port");
        if (port == null) {
            port = 27017;
        }

        dbName = config.getString("database");
        collectionName = config.getString("collection");
        monitorCollName = config.getString("monitor");
        delayAmount = config.getInt("delay");
        if (delayAmount == null || delayAmount == 0) {
            delayAmount = 10;
        }

        wordFile = new File(config.getString("words"));
        clientLogFile = new File(config.getString("clientLog"));
        serverLogFile = new File(config.getString("serverLog"));
        queryWordFile = new File(config.getString("wordFilter"));
    }


    public static ArrayList<String> createDictionary(File file) {
        ArrayList<String> toReturn = new ArrayList<>();
        Scanner scan = null;
        try {
            scan = new Scanner(file);
        } catch (Exception e) {
            System.err.println("Invalid dictionary file.");
            System.exit(0);
        }

        while (scan.hasNext()) {
            toReturn.add(scan.next());
        }

        return toReturn;
    }

    private static void throwError(String error) {
        System.out.println(error);
        System.exit(0);
    }
}

